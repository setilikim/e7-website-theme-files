<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sQrt121
 * Date: 9/16/13
 * Time: 2:36 PM
 * To change this template use File | Settings | File Templates.
 */


include_once( 'MorpheusShortCodeScriptLoader.php' );

class MorpheusShortcodeCareers extends MorpheusShortCodeScriptLoader {
	static $addedAlready = false;

	public function handleShortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'width'              => 4,
			'medium_width'       => 6,
			'font_size'          => '16px',
			'number'             => - 1,
			'class'              => ''
		), $atts ) );

		$Qargs = array(
			'post_type'               => 'coll-careers',
			'posts_per_page'          => $number
		);

		$output = '';


		// items
		$loop = new WP_Query( $Qargs );
		while ( $loop->have_posts() ) : $loop->the_post();
			global $post;

			$title = get_the_title( get_the_ID() );
			$url = get_permalink( get_the_ID() );

			$output .= "<a href='". $url ."'>";
			$output .= $title;
			$output .= '</a>';
		endwhile; //end items


		wp_reset_postdata();

		return $output;


	}

	public function addScript() {
		if ( ! self::$addedAlready ) {
			self::$addedAlready = true;
		}
	}

}


$sc = new MorpheusShortcodeCareers();
$sc->register( 'coll_careers' );