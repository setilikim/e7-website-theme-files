<?php get_header(); ?>
<?php
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();

		// thumbnail
		if ( has_post_thumbnail() ) {
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		} else {
			$thumb = get_post_meta( $post->ID, 'thumb', true );
			$thumb = wp_get_attachment_image_src( $thumb, 'full' );
		}
		$outputImage = '<img width=" ' . $thumb[1] . ' " height=" ' . $thumb[2] . ' " src=" ' . $thumb[0] . ' " />';
		


		// position
		$position = get_post_meta( $post->ID, 'coll_position', true );
		$outputPosition = '';
		if (!empty($position)) {
			$outputPosition .= '<h3 class="subtitle-text"> '. $position .' </h3>';
		}


		// social media
		$outputSocial = '';
		$facebook = get_post_meta( $post->ID, 'coll_facebook', true );
		$twitter = get_post_meta( $post->ID, 'coll_twitter', true );
		$linkedin = get_post_meta( $post->ID, 'coll_linkedin', true );
		$googleplus = get_post_meta( $post->ID, 'coll_googleplus', true );
		$bitbucket = get_post_meta( $post->ID, 'coll_bitbucket', true );
		$github = get_post_meta( $post->ID, 'coll_github', true );

		if ( $facebook || $twitter || $linkedin || $googleplus || $bitbucket || $github ) {
			$outputSocial .= '<ul class="icons">';

			if ($facebook) {
				$outputSocial .= '<li><a class="link" target="_blank" href=" '. $facebook . ' "><i class="fa fa-facebook"></i></a></li> ';
			}

			if ($twitter) {
				$outputSocial .= '<li><a class="link" target="_blank" href=" ' .$twitter .' "><i class="fa fa-twitter"></i></a></li> ';
			}

			if ($linkedin) {
				$outputSocial .= '<li><a class="link" target="_blank" href=" '. $linkedin .' "><i class="fa fa-linkedin"></i></a></li> ';
			}

			if ($googleplus) {
				$outputSocial .= '<li><a class="link" target="_blank" href=" '. $googleplus .' "><i class="fa fa-google-plus"></i></a></li> ';
			}

			if ($bitbucket) {
				$outputSocial .= '<li><a class="link" target="_blank" href=" '. $bitbucket. ' "><i class="fa fa-bitbucket"></i></a></li> ';
			}

			if ($github) {
				$outputSocial .= '<li><a class="link" target="_blank" href=" '. $github. ' "><i class="fa fa-github"></i></a></li> ';
			}
			
			$outputSocial .= '</ul>';
		}
		
		?>

		<div class="wrapper common coll-single" id="skrollr-body">
		<section class="title-container js-coll-page-section coll-page-section">
			<div class="row">
				<div class="large-12 columns">
					<div class="coll-section-divider title-divider">
						<span class="text large-2 medium-2"><?php _e( 'employee', 'framework' ); ?></span>
						<span class="line large-10 medium-10"><span class="color"></span></span>
					</div>

					<div class="title-wrapper clearfix">
						<div class="small-7 medium-3 large-3 columns">
							<?php echo $outputImage ;?>
						</div>
						<div class="small-12 medium-9 large-9 columns">
							<h1 class="title-text"><?php the_title(); ?></h1>
							<?php echo $outputPosition; ?>
							<?php echo $outputSocial; ?>
						</div>
					</div>
				</div>
			</div>
		</section>



		<section class="js-coll-page-section coll-page-section">
			<div class="row">
				<div class="large-12 columns">
					<div class="coll-section-divider content-divider clearfix">
						
						<span class="line large-offset-2 large-10 medium-10"><span class="color"></span></span>
					</div>
					
				</div>
			</div>
		</section>

		<!-- <section class="js-coll-page-section coll-page-section">
			<div class="row">
				<div class="large-12 columns">
					<div class="coll-section-divider content-divider clearfix">
						<span class="text large-2 medium-2"><?php _e( 'biography', 'framework' ); ?></span>
						<span class="line large-10 medium-10"><span class="color"></span></span>
					</div>
					<div class="copy-container large-10 large-offset-2 medium-10 medium-offset-2">
						<div class="content-wrapper">
							<article class="entry-content">
								<?php the_content(); ?>
							</article>
						</div>
					</div>
				</div>
			</div>
		</section> -->

	<?php
	endwhile;
endif; ?>
<?php get_footer(); ?>